#ifndef EEPROM_H
#define EEPROM_H

extern unsigned char eep_greet[];
extern unsigned char shell_menu[3][10];

unsigned char eep_rd(unsigned char *ptr);

void eep_wr(unsigned char *ptr, unsigned char val);

#endif

