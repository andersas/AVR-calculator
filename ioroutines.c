#include <avr/io.h>

#define sei()   __asm__ __volatile__ ("sei" ::)
#define cli()   __asm__ __volatile__ ("cli" ::)

#include "syscalls.h"
#include "calc.h"
#include "data.h"
#include "ioroutines.h"


// Retrieves a keyboard input press and translates it into
// two coordinates (column,row)
// (if the alternate mode is enabled, eg key 0,0 was pressed
// before, then we add 5 to row)

// it then sends the keypress on to whatever process
// has the current screen, or discards it if none is listening.
// changes current screen on alt-< or alt->
// and sends a SIG_BREAK to the active process if
// BRK key was pressed


void __attribute__ ((__noreturn__)) wait_keyb(pid_t parent) {

	unsigned char alt;  // Alternate function
	unsigned char row, col;
	unsigned char i;
	pid_t screen_pid;

	alt = 0;

	KEYB_BUF = _KEY_READ;

	TASK_SIGNALS[SYS_GETPID()] = (sig_ptr) 0x0000;

inf_loop:
	while (1) {
	sei();

	row = KEYB_BUF;

	if (row == _NOKEY || row == _KEY_READ) {
		KEYB_BUF = _KEY_READ;
		// If no new key was delivered to us by the keyboard
		SYS_SLEEP((char) 2); // wait a bit and try again
	} else {

		KEYB_BUF = _KEY_READ;
		// Decode the keyboard "scancode"
		col = row;
		row &= 0x0F;
		col >>= 4;
		row++;      // Add 1 to get bit pattern of row/column scan
		col++;

		// Then translate the row/column scan into
		// row/column number (by taking log_2(scan))

		for (i = 0; row >>= 1; i++);
		row = i;
		for (i = 0; col >>= 1; i++);
		col = i;

		row += alt;  // Add the alternate function

		// If the ALT button was pressed, toggle ALT:
		if  (col == 0) {
			if (row == 0) { PORTD |= 32; alt = 5; break;}
			else if (row == 5) { PORTD &= ~32; alt = 0; break;}
		}

		// The last key pressed at this point was not ALT,
		// so clear it:
		alt = 0;
		PORTD &= ~32;

		// First check if we need to switch screen:
		cli();
		if (row == 5) {
			if (col == 1) {
				if (CURR_SCREEN == 0)
					screenSwitch((unsigned char) (_NSCREENS-1));
				else
					screenSwitch((unsigned char) (CURR_SCREEN-1));
				break;
			} else if (col == 2) {
				screenSwitch((unsigned char) ((CURR_SCREEN+1) % _NSCREENS));
				break;
			}
		}

		if (CURR_SCREEN >= _NSCREENS) break; // No program at
							// current screen.
		screen_pid = SCREEN_PIDS[CURR_SCREEN];
		if (screen_pid > _MAX_TASKS) break; // still no program


		// Last thing to check if is the BRK button was pressed.
		// In that case, raise the process:
		
		if (col == 0 && row == 9) {
			SYS_RAISE((unsigned char) _SIG_BREAK, screen_pid);
		} else {
		// Set the screen keypress
			KEYPRESS[CURR_SCREEN].col = col;
			KEYPRESS[CURR_SCREEN].row = row;
		// And signal the process.
			SYS_RAISE((unsigned char) _SIG_KEYB, screen_pid);
		}
		
		break;

	}
	}

	// Cleanup routine: Wait till the user lets go of key:
	while (KEYB_BUF != _NOKEY) {
		sei();
		if (KEYB_BUF == _NOKEY) break;
		KEYB_BUF = _KEY_READ;
		SYS_SLEEP((unsigned char) 1);
	}
	goto inf_loop;
	


}

void screenSwitch(unsigned char screen_num) {

	unsigned char i;

	if (screen_num >= _NSCREENS) return;

	for (i = (unsigned char) 0;i<_DISPBUF_LEN; SCREENS[CURR_SCREEN][i] = DISPLAY_DATA[i],i++);

	CURR_SCREEN = screen_num;
	for (i = 0;i<_DISPBUF_LEN; DISPLAY_DATA[i] = SCREENS[CURR_SCREEN][i],i++);
	return;
}


// set position pos of process screen to value val.
void dispWrite(unsigned char pos, unsigned char val) {

	pid_t pid;
	unsigned char screen;
	pid = SYS_GETPID();

	if (pos >= _DISPBUF_LEN) return;

	cli();
	if (SCREEN_PIDS[CURR_SCREEN] == pid) {
		DISPLAY_DATA[pos] = val;
	} else {
		// find the process' screen
		screen = getscreen();

		// If the process has a screen associated with it
		if (screen != _NSCREENS) {
			SCREENS[screen][pos] = val;
		}

	}
	sei();
}

unsigned char getscreen() {
	unsigned char screen;
	pid_t pid;

	pid = SYS_GETPID();
	for (screen = 0; screen < _NSCREENS; screen++) {
		if (SCREEN_PIDS[screen] == pid) break;
	}

	return screen;
}

unsigned char idiv10(unsigned char dividend) {

	dividend = (((dividend * 205) & 0xFF00) >> 8);
	return dividend >> 3; // The assembler code generated
			      // differs from >> 11
}

//unsigned char idiv60(unsigned char dividend) {
//
//	dividend = (((dividend * 137) & 0xFF00) >> 8);
//	return dividend >> 5;
//}

