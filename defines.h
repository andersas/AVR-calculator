
; Ram endings

.equ RAMEND, 0x045F ; 32 registers, 64 I/O ports and 1024 memory locations
.equ STACKLOW, 0x03FB  ; Lowest stack pointer

; Ram areas

.equ DISPBUF_LEN, 10
.equ NBCDREG, 6		  ; Number of BCD registers
.equ BCDREG_LEN, 8


; OS

.equ STACK_DEPTH, 81 ; 49 ; 49 bytes of stack per process
.equ MAX_TASKS, 6 ; 10   ; gives (49 - 32 - 1(SREG) - 2 (return addr))  / 2
		     ; = 7 nested function calls per process.

.equ MAX_TICKS, 20   ; number of clock ticks in a process timeslice

.equ META_PRIO, 0x03 ; bits 0 and 1 gives task priority (in meta byte)
.equ META_STATE, 0x0C ; bits 2 and 3 give task state
.equ STATE_SLEEPS,0x04
.equ STATE_SLEEPS_BITNO, 2
.equ STATE_STOPPED, 0x08
.equ STATE_WAITS, 0x10
.equ STATE_SIGPEND, 0x40  ; Signals are pending
.equ STATE_SIGPEND_BITNO, 6
.equ STATE_SIGS, 0x80     ; Signals are being handled
.equ STATE_SIGS_BITNO, 7

.equ PRIO_IDLE, 0
.equ PRIO_LOW, 1
.equ PRIO_NORMAL, 2
.equ PRIO_HIGH, 3

; Signals
.equ SIG_CHLD, 0 ; Program receives this signal when child dies.
.equ SIG_BREAK, 1 ; Program receives this signal if interrupted or on ALT+ent.
.equ SIG_ALARM, 2 ; Program receives this on exiting sleep
.equ SIG_KEYB, 3  ; Program receives this on keypress update

.equ NSCREENS, 4 ; Number of virtual screens


; Keyboard
.equ NOKEY, 0x22     ; Keycode for no key pressed
.equ KEY_READ, 0x44 ; Signal for the keyboard handler that last key was read.

; 7-segment values:
.equ SEG_OFF, 0b11111111
.equ SEG_0, 0b11000000
.equ SEG_1, 0b11111001
.equ SEG_2, 0b10100100
.equ SEG_3, 0b10110000
.equ SEG_4, 0b10011001
.equ SEG_5, 0b10010010
.equ SEG_6, 0b10000010
.equ SEG_7, 0b11111000
.equ SEG_8, 0b10000000
.equ SEG_9, 0b10010000
.equ SEG_DOT, 0b01111111
.equ SEG_A, 0b10001000
.equ SEG_B, 0b10000011
.equ SEG_C, 0b11000110
.equ SEG_D, 0b10100001
.equ SEG_E, 0b10000110
.equ SEG_F, 0b10001110
.equ SEG_G, 0b11000010
.equ SEG_H, 0b10001001
.equ SEG_I, 0b11111001
.equ SEG_J, 0b11100001
.equ SEG_K, 0b10010110
.equ SEG_L, 0b11000111
.equ SEG_M, 0b10101010
.equ SEG_N, 0b10101011
.equ SEG_O, 0b10100011
.equ SEG_P, 0b10001100
.equ SEG_Q, 0b10010100
.equ SEG_R, 0b10101111
.equ SEG_S, 0b10011011
.equ SEG_T, 0b10000111
.equ SEG_U, 0b11000001
.equ SEG_V, 0b11010101
.equ SEG_W, 0b10010101
.equ SEG_X, 0b10110110
.equ SEG_Y, 0b10010001
.equ SEG_Z, 0b10101101
.equ SEG_MINUS, 0b10111111


; Internal Oscillator calibrator values
.equ OSCCAL_MAX, 0xFF
.equ OSCCAL_MIN, 0x00
.equ OSCCAL_8MHZ, 0xb3

; I/O ports 
.equ PIND, 0x10
.equ DDRD, 0x11
.equ PORTD, 0x12
.equ PINC, 0x13
.equ DDRC, 0x14
.equ PORTC, 0x15
.equ PINB, 0x16
.equ DDRB, 0x17
.equ PORTB, 0x18
.equ PINA, 0x19
.equ DDRA, 0x1A
.equ PORTA, 0x1B
.equ EECR, 0x1C
.equ EEDR, 0x1D
.equ EEARL, 0x1E
.equ EEARH, 0x1F

.equ WDTCR, 0x21
.equ ASSR, 0x22
.equ OCR2, 0x23
.equ TCNT2, 0x24
.equ TCCR2, 0x25
.equ OCR1BL, 0x28  ; Output compare 1B
.equ OCR1BH, 0x29
.equ OCR1AL, 0x2A  ; Output compare 1A
.equ OCR1AH, 0x2B
.equ TCNT1L, 0x2C  ; Timer/counter 1 counter register
.equ TCNT1H, 0x2D
.equ TCCR1B, 0x2E ; Timer/counter control register 1B / 1A
.equ TCCR1A, 0x2F
.equ SFIOR, 0x30 ; Special function I/O register
.equ OSCCAL, 0x31 ; OSCillator Calibration
.equ TCNT0, 0x32 ; Timer 0 current current value
.equ TCCR0, 0x33 ; Timer 0 control register
.equ MCUCR, 0x35
.equ TIMSK, 0x39
.equ OCR0, 0x3C
.equ SPL, 0x3D ; Stack
.equ SPH, 0x3E 
.equ SREG, 0x3F ; Status register

; Bit values in certain registers

.equ FOC0, 7 ; timer 0
.equ WGM00, 6
.equ WGM01, 3
.equ COM00, 4
.equ COM01, 5
.equ CS00, 0
.equ CS01, 1
.equ CS02, 2
.equ OCIE0, 1 ; interrupt bits

.equ WGM12, 3  ; timer 1
.equ CS10, 0
.equ CS11, 1
.equ CS12, 2
.equ OCIE1A, 4
.equ OCIE1B, 3
.equ SE, 6
.equ SM2, 7
.equ SM1, 5
.equ SM0, 4

.equ WGM20, 6 ; timer 2
.equ FOC2, 7
.equ COM21, 5
.equ COM20, 4
.equ WGM21, 3
.equ CS22, 2
.equ CS21, 1
.equ CS20, 0
.equ OCIE2, 7

.equ AS2, 3 ; Asynchronous timer 2

; EEPROM control register
.equ EERE, 0  ; Read enable
.equ EEWE, 1  ; write enable
.equ EEMWE, 2  ; master write enable
.equ EERIE, 3  ; ready interrupt

