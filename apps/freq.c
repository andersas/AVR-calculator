#include <avr/io.h>
#include "../data.h"
#include "../calc.h"
#include "../ioroutines.h"
#include "../display_helper.h"


void freqhndlr(unsigned char signo) {

	unsigned char ovrflow;
	unsigned char dig1, dig2, dig3;

	if (signo == _SIG_KEYB) {
		ovrflow = OCR0;

		if (KEYPRESS[getscreen()].row == 0 && KEYPRESS[getscreen()].col == 1) {
			ovrflow--;
			if (ovrflow < 8) ovrflow = 255;
			OCR0 = ovrflow;
		} else if (KEYPRESS[getscreen()].row == 0 && KEYPRESS[getscreen()].col == 2) {

			if (ovrflow == 255) ovrflow = 7;
			ovrflow++;
			OCR0 = ovrflow;
		}

		dig2 = idiv10(ovrflow);
		dig3 = idiv10(dig2);
		dig1 = ovrflow - 10*dig2; // remainders
		dig2 = dig2 - 10*dig3;

		dispWrite((unsigned char) 0, display_xlate(dig3));
		dispWrite((unsigned char) 1, display_xlate(dig2));
		dispWrite((unsigned char) 2, display_xlate(dig1));

	}

}

void __attribute__ ((__noreturn__)) freq(pid_t pid) {

	unsigned char i;

	for (i = 0; i<_DISPBUF_LEN; i++) {
		dispWrite(i,(unsigned char) _SEG_0);
	}
		dispWrite((unsigned char) 2, (unsigned char) _SEG_8);
		dispWrite((unsigned char) 3, (unsigned char) _SEG_OFF);
	
	SYS_SETSIG(&freqhndlr);

	for (;;SYS_SLEEP((unsigned char) 254));

}

