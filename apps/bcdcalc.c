#include <string.h>

#include "../data.h"
#include "../ioroutines.h"
#include "../calc.h"
#include "../display_helper.h"

#define POINT 10

#define NONE 0
#define ADD 1
#define MUL 1
#define SUB 2
#define DIV 2


void enter();
void add();
void mul();
void div();
void sub();
void add_digit(unsigned char dig);
void readout();
void clr();
void pass();

unsigned char abs(signed char num);


register struct {
	unsigned crunching : 1; // if the calc program is currently calculating
	unsigned last_pred1_op : 2;
	unsigned last_pred2_op : 2;
	unsigned point_sat : 1;     // indicates decimal point has been sat
	unsigned garbage : 1;       // garbage on top of stack,
				    // clear it before adding appending
				    // new digits
} status asm("r2");
register unsigned char sp asm("r3"); // BCD Stack pointer
register void (*op)() asm("r4"); // and (hopefully) r5
register unsigned char bcdp asm("r6"); // bcd input pointer

void bcdsighndlr(unsigned char signo) {

  unsigned char append;

  switch (signo) {

	case _SIG_KEYB:
	  if (status.crunching == 1) break;
	  append = -1;
	  op = &pass;
	  switch (KEYPRESS[getscreen()].col*10 + KEYPRESS[getscreen()].row) {
	   case 14: append = 0; break;  // numeric keypad:
	   case 24: append = POINT; break;
	   case 13: append = 1; break;
	   case 12: append = 4; break;
	   case 11: append = 7; break;
	   case 23: append = 2; break;
	   case 22: append = 5; break;
	   case 21: append = 8; break;
	   case 33: append = 3; break;
	   case 32: append = 6; break;
	   case 31: append = 9; break;


	   case 40: op = &div; break; // arithmetic
	   case 41: op = &mul; break;
	   case 42: op = &sub; break;
	   case 43: op = &add; break;
	   case 44: op = &enter; break; // enter was pressed

	   case 99: op = &clr; break;


	   default: break;
	  }
	  if (append != -1) {
		if (bcdp | status.garbage)
			add_digit(append);
	  } 

	  status.crunching = 1;
	  SYS_START();
	break;

	case _SIG_BREAK:

	break;


	default:
	break;
  }


}

void __attribute__ ((__noreturn__)) bcdcalc(pid_t pid) {

	unsigned int i;

	status.crunching = 1;
	SYS_SETSIG(&bcdsighndlr);
	status.last_pred1_op = status.last_pred2_op = NONE;
	status.point_sat = 0;
	status.garbage = 1;
	sp = bcdp = 0; // stack & bcdreg pointer.

	memset(&BCDREG[0], 0, sizeof(BCD_register));
	BCDREG[0].sign = 1;

	while(1) {
		readout();
		status.crunching = 0;
		SYS_STOP(); // Stop and wait for a signal
		if (status.crunching == 0) continue; // See if we need to work

		
		(*op)();

	}
}

void clr() { // clear top of stack
	memset(&BCDREG[sp], 0, sizeof(BCD_register));
	BCDREG[sp].sign = 1;
	status.garbage = status.point_sat = 0;
}

void add_digit(unsigned char dig) {

	if (status.garbage) {
		clr();
	}

	if (dig == 0 && bcdp == 0) return; // ignore 0 on first entry

	if (dig == POINT)
		status.point_sat = 1;
	if (!status.point_sat) {
		if (BCDREG[sp].exponent >= 0) {
		 BCDREG[sp].exponent++;
		} else {
		 BCDREG[sp].exponent--;
		}
	}

	BCDREG[sp].mantissa[bcdp++] = dig;

}

void pass() {}  // no operation

void enter() {


}

void add() {

}

void mul() {

}

void div() {

}
void sub() {

}

void readout() { // Reads out top of BCD stack to display

	unsigned int i,c;
	dispWrite(0, display_xlate(BCDREG[sp].mantissa[0]) & _SEG_DOT);
	for (i = 1; i<_DISPBUF_LEN-2 ; i++) {
		dispWrite(i, display_xlate(BCDREG[sp].mantissa[i]));
	}

	i = idiv10(abs(BCDREG[sp].exponent));
	c = abs(BCDREG[sp].exponent) - i*10;

	if (abs(BCDREG[sp].exponent) < 10) {
		dispWrite(_DISPBUF_LEN-2, (BCDREG[sp].exponent<0) ? _SEG_MINUS : _SEG_OFF);
		dispWrite(_DISPBUF_LEN-1, display_xlate(c));
	} else {
		dispWrite(_DISPBUF_LEN-2, display_xlate(i) & (BCDREG[sp].exponent<0) ? _SEG_DOT : _SEG_OFF);
		dispWrite(_DISPBUF_LEN-1, display_xlate(c) & (BCDREG[sp].exponent<0) ? _SEG_DOT : _SEG_OFF);
	}

}

unsigned char abs(signed char num) {
	if (num < 0) {
		return 0xFF - num + 1;
	} else {
		return num;
	}
}
