#include "calc.h"
#include "syscalls.h"

typedef struct {
	void (*process)(unsigned char pid);
	unsigned char meta_byte;
	unsigned char ticks;
} context;

typedef void (*sig_ptr)(unsigned char);

typedef struct {
	signed   char sign;
	signed   char exponent;
	unsigned char mantissa[_BCDREG_LEN]; // add another 2 hidden digits
} BCD_register;

typedef struct {
	unsigned char col;
	unsigned char row;
} keypress;

// read comments in data.asm file
extern volatile unsigned char DISPLAY_DATA[_DISPBUF_LEN];
extern volatile unsigned char ActiveSegment;
extern volatile unsigned char KEYBI_COUNT;
extern volatile unsigned char KEYBI_CODE;
extern volatile unsigned char KEYB_BUF;
extern volatile pid_t CURR_PID;
extern volatile unsigned char TICKS;
extern volatile context TASK_CONTEXT[_MAX_TASKS];
extern volatile pid_t TASK_PARENT[_MAX_TASKS];
extern volatile sig_ptr TASK_SIGNALS[_MAX_TASKS];
extern volatile unsigned char SIGNAL_BITMASK[_MAX_TASKS];
extern volatile int SYS_TIME;
extern volatile unsigned char SCREENS[_NSCREENS][_DISPBUF_LEN];
extern volatile unsigned char CURR_SCREEN;
extern volatile pid_t SCREEN_PIDS[_NSCREENS];
extern volatile keypress KEYPRESS[_NSCREENS];

// BCD registers
extern BCD_register BCDREG[_NBCDREG];






