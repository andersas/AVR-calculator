.include "defines.h"

.section .text

; Some helper applications for the display

display_xlate:	.global display_xlate
	; translates a number between 0 and 9 in r24 to
	; a segment value for that number

	clr r25
	cpi r24, 10
	brsh xlate_err

	ldi r30, lo8(pm(xlate))
	ldi r31, hi8(pm(xlate))
	lsl r24     ; Each entry takes 2 instructions
	add r30, r24
	clr r24
	adc r31, r24

	ijmp

xlate:	ldi r24, SEG_0
	ret
	ldi r24, SEG_1
	ret
	ldi r24, SEG_2
	ret
	ldi r24, SEG_3
	ret
	ldi r24, SEG_4
	ret
	ldi r24, SEG_5
	ret
	ldi r24, SEG_6
	ret
	ldi r24, SEG_7
	ret
	ldi r24, SEG_8
	ret
	ldi r24, SEG_9
	ret
xlate_err:
	ldi r24, 0b01111110
	ret



scroll_msg:	.global scroll_msg
	; Scroll a message over the display

	; r25:r24: pointer to message
	; r22: message length
	; r20: delay between each scroll measured in ticks

	mov r23, r22
	subi r23, -8 ; Add another 8 SEG_OFF's to the message
	movw r26, r24 ; Put the pointer in X and work with it there
	
scrollloop:
	ldi r30, lo8(DISPLAY_DATA)
	ldi r31, hi8(DISPLAY_DATA)
	; Scroll display:
	ldi r18, 7
disploop:
	ldd r19, Z+1
	st Z+, r19
	dec r18
	brne disploop
	
	; Add to display from buffer. If we haven't used all buffer yet,
	; get from there, otherwise store a blank segment.

	ldi r19, SEG_OFF
	tst r22
	breq st_last

	dec r22 ; Take from buffer
	ld r19, X+

st_last:st Z, r19
	dec r23

scroll_sleep:
	; save variables:
	push r26
	push r27
	push r22
	push r23
	push r20
	; Sleep the prescribed time:
	mov r24, r20
	rcall SYS_SLEEP
	pop r20
	pop r23
	pop r22
	pop r27
	pop r26

	tst r23
	brne scrollloop

scroll_end:
	ret	


