.include "defines.h"

.section .bss
.org 0x0000

DISPLAY_DATA: 	.global DISPLAY_DATA  
	.skip DISPBUF_LEN  ; display buffer
ActiveSegment: 	.global ActiveSegment
	.skip 1
KEYBI_COUNT: 	.global KEYBI_COUNT
	.skip 1  ; Internal keyboard handler buffer
KEYBI_CODE: 	.global KEYBI_CODE
	.skip 1  ; Internal keyboard handler buffer
KEYB_BUF: 	.global KEYB_BUF
	.skip 1  ; Keyboard buffer
OS_DATA:	.global OS_DATA
CURR_PID:	.global CURR_PID
	.skip 1  ; Current Process ID
TICKS:		.global TICKS
	.skip 1	; how many ticks a process has left of its timeslice
TASK_CONTEXT:	.global TASK_CONTEXT
	.skip 4*MAX_TASKS ; stack pointer of each process
			  ; task meta byte + wakeup time
TASK_PARENT:	.global TASK_PARENT
	.skip MAX_TASKS		; Table of each tasks parent
TASK_SIGNALS:	.global TASK_SIGNALS
	.skip 2*MAX_TASKS ; Pointers to signal handlers
SIGNAL_BITMASK:	.global SIGNAL_BITMASK
	.skip MAX_TASKS ; Bitmask of what enabled signals are waiting
SYS_TIME:	.global SYS_TIME
	.skip 2		  ; System clock
OS_DATA_END:	.global OS_DATA_END
SCREENS:	.global SCREENS
	.skip NSCREENS*DISPBUF_LEN  ; virtual screen buffers
CURR_SCREEN:	.global CURR_SCREEN
	.skip 1
SCREEN_PIDS: 	.global SCREEN_PIDS ; pid associated with each screen
	.skip NSCREENS
KEYPRESS:	.global KEYPRESS    ; keypress waiting for each process.
	.skip 2*NSCREENS


BCDREG_START: 	.global BCDREG_START
BCDREG: 	.global BCDREG
	.skip NBCDREG*BCDREG_LEN


end:






