.include "defines.h"

.section .text

init_sleepmode: .global init_sleepmode

	cli
	in r16, MCUCR
	ori r16, (1 << SE)  ; Enable sleep
	andi r16, 0xFF-((1 << SM0) | (1 << SM1) | (1 << SM2)) ; select idle mode
	out MCUCR, r16	
	sei

	ret
