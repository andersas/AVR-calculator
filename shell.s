	.file	"shell.c"
__SREG__ = 0x3f
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__tmp_reg__ = 0
__zero_reg__ = 1
	.global __do_copy_data
	.global __do_clear_bss
	.text
.global	shell
	.type	shell, @function
shell:
	push r29
	push r28
	push __tmp_reg__
	in r28,__SP_L__
	in r29,__SP_H__
/* prologue: function */
/* frame size = 1 */
	ldi r22,lo8(0)
	rjmp .L2
.L3:
	subi r22,lo8(-(1))
.L2:
	mov r16,r22
	ldi r17,lo8(0)
	movw r24,r16
	subi r24,lo8(-(eep_greet))
	sbci r25,hi8(-(eep_greet))
	std Y+1,r22
	call eep_rd
	movw r30,r16
	subi r30,lo8(-(BCDREG))
	sbci r31,hi8(-(BCDREG))
	st Z,r24
	ldd r22,Y+1
	tst r24
	brne .L3
	ldi r24,lo8(BCDREG)
	ldi r25,hi8(BCDREG)
	ldi r20,lo8(12)
	call scroll_msg
	clr r6
	clr r14
	clr r15
	ldi r17,lo8(10)
.L4:
	mul r6,r17
	movw r24,r0
	clr r1
	add r24,r14
	adc r25,r15
	subi r24,lo8(-(shell_menu))
	sbci r25,hi8(-(shell_menu))
	call eep_rd
	mov r22,r24
	mov r24,r14
	call dispWrite
	sec
	adc r14,__zero_reg__
	adc r15,__zero_reg__
	ldi r18,lo8(10)
	cp r14,r18
	cpc r15,__zero_reg__
	brne .L4
	ldi r24,lo8(gs(shellsighndlr))
	ldi r25,hi8(gs(shellsighndlr))
	call SYS_SETSIG
	call SYS_GETPID
	mov r5,r24
	mov r4,r24
	mov r3,r24
.L5:
	ldi r24,lo8(-6)
	call SYS_SLEEP
	rjmp .L5
	.size	shell, .-shell
.global	shellsighndlr
	.type	shellsighndlr, @function
shellsighndlr:
	push r17
	push r28
	push r29
/* prologue: function */
/* frame size = 0 */
	cpi r24,lo8(3)
	breq .+2
	rjmp .L20
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ld r24,Z
	cpi r24,lo8(4)
	breq .+2
	rjmp .L11
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ldd r17,Z+1
	cpi r17,lo8(4)
	breq .+2
	rjmp .L11
	mov r17,r3
	call SYS_GETPID
	cp r17,r24
	brne .L12
	tst r6
	brne .L12
	ldi r24,lo8(gs(bcdcalc))
	ldi r25,hi8(gs(bcdcalc))
	ldi r22,lo8(3)
	ldi r20,lo8(0)
	call SYS_new_process
	mov r3,r24
	sts SCREEN_PIDS+1,r24
	ldi r24,lo8(1)
	rjmp .L23
.L12:
	ldi r18,lo8(1)
	cp r6,r18
	brne .L13
	mov r17,r4
	call SYS_GETPID
	cp r17,r24
	brne .L13
	ldi r24,lo8(gs(freq))
	ldi r25,hi8(gs(freq))
	ldi r22,lo8(2)
	ldi r20,lo8(0)
	call SYS_new_process
	mov r4,r24
	sts SCREEN_PIDS+2,r24
	ldi r24,lo8(2)
.L23:
	call screenSwitch
	rjmp .L20
.L13:
	ldi r19,lo8(2)
	cp r6,r19
	brne .L14
	mov r17,r5
	call SYS_GETPID
	cp r17,r24
	brne .L14
	ldi r24,lo8(gs(clock))
	ldi r25,hi8(gs(clock))
	ldi r22,lo8(2)
	ldi r20,lo8(0)
	call SYS_new_process
	mov r5,r24
	sts SCREEN_PIDS+3,r24
	ldi r24,lo8(3)
	rjmp .L23
.L14:
	ldi r20,lo8(3)
	cp r6,r20
	breq .+2
	rjmp .L20
/* #APP */
 ;  48 "shell.c" 1
	ldi r16, 0b00001000
	out 33, r16
	end: rjmp end
	
 ;  0 "" 2
/* #NOAPP */
	rjmp .L20
.L11:
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ld r21,Z
	cpi r21,lo8(1)
	brne .L15
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ldd r22,Z+1
	tst r22
	brne .L15
	tst r6
	brne .L16
	ldi r18,lo8(4)
	mov r6,r18
.L16:
	dec r6
	ldi r28,lo8(0)
	ldi r29,hi8(0)
	ldi r17,lo8(10)
.L17:
	mul r6,r17
	movw r24,r0
	clr r1
	add r24,r28
	adc r25,r29
	subi r24,lo8(-(shell_menu))
	sbci r25,hi8(-(shell_menu))
	call eep_rd
	mov r22,r24
	mov r24,r28
	call dispWrite
	adiw r28,1
	cpi r28,10
	cpc r29,__zero_reg__
	brne .L17
	rjmp .L20
.L15:
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ld r23,Z
	cpi r23,lo8(2)
	brne .L20
	call getscreen
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	ldd r25,Z+1
	tst r25
	brne .L20
	mov r26,r6
	subi r26,lo8(-(1))
	mov r6,r26
	cpi r26,lo8(4)
	brlo .L18
	clr r6
.L18:
	ldi r28,lo8(0)
	ldi r29,hi8(0)
	ldi r17,lo8(10)
.L19:
	mul r6,r17
	movw r24,r0
	clr r1
	add r24,r28
	adc r25,r29
	subi r24,lo8(-(shell_menu))
	sbci r25,hi8(-(shell_menu))
	call eep_rd
	mov r22,r24
	mov r24,r28
	call dispWrite
	adiw r28,1
	cpi r28,10
	cpc r29,__zero_reg__
	brne .L19
.L20:
/* epilogue start */
	pop r29
	pop r28
	pop r17
	ret
	.size	shellsighndlr, .-shellsighndlr
