#ifndef _IOROUTINES
#define _IOROUTINES

// routine to set the current screen:

void screenSwitch(unsigned char screen_num);

// routine to write to the current screen 
void dispWrite(unsigned char pos, unsigned char val);

unsigned char getscreen(); // Returns process screen number,
			   // or _NSCREENS if the process has no
			   // screen associated with it.

// Division routines

unsigned char idiv10(unsigned char dividend);


#endif
