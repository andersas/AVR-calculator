.include "defines.h"

.section .text

.equ ARG0H, 25
.equ ARG0L, 24
.equ ARG1H, 23
.equ ARG1L, 22
.equ ARG2H, 21
.equ ARG2L, 20
.equ ARG3H, 19
.equ ARG3L, 18
.equ ARG4H, 17
.equ ARG4L, 16
.equ ARG5H, 15
.equ ARG5L, 14
.equ ARG6H, 13
.equ ARG6L, 12
.equ ARG7H, 11
.equ ARG7L, 10
.equ ARG8H, 9
.equ ARG8L, 8

; child_pid = SYS_new_process(ptr proc, char state [, sleep ticks])
.global SYS_new_process ; Create a new process pointed to by r25:r24
	; 		; The new process gets parants PID in r25:r24.
	; Arguments: r22: process state (including priority)
	; 	     r20: if the process state is sleeping,
	; 	          this argument specifies for how many ticks.
	;
	; 	Returns: r25:24 PID of new process (-1 on failure)
	; 	r1 = 0, may clobber r0 and r18-27 + r30-31
SYS_new_process:

	ldi r18, 0
	ldi r30, lo8(TASK_CONTEXT)  ; Find an unused proces ID
	ldi r31, hi8(TASK_CONTEXT)

	cli
test_loop:
	ld r26, Z	; Test if we have a zero stack pointer
	ldd r27, Z+1	; (= no task)
	adiw r26, 0
	breq thisone
	inc r18
	adiw r30, 4 ; Go to next entry of TASK_CONTEXT array
	cpi r18, MAX_TASKS
	brne test_loop

failure:
	sei 
	ser ARG0H ; r25:r24 = -1 signals failure 
	ser ARG0L
	ret

thisone:
	movw r26, r30  ; Save the found context /pointer/
	ldi r19, STACK_DEPTH
	mul r18, r19           ; curr pid * stack depth
	ldi r30, lo8(RAMEND-35)   ; Figure out where this process 
	ldi r31, hi8(RAMEND-35)   ; should store its stack
	sub r30, r0 ; the 35 is for 32 registers, 1 status reg + ``return addr''
	sbc r31, r1

		       ; Caller supplies entry point in r25:r24.
	std Z+35, ARG0L; Store this ``return address'' to end of process' stack
	std Z+34, ARG0H; low byte first (stacks grow downward)
	clr r1 ; Make sure the process status flag has no wrong bits set
	std Z+32, r1  ; (disable global interrupts inside scheduler)
		      ; This also clears r1 to zero again
	; Store the parents PID in r25:r24 of the new process:
	lds r19, CURR_PID
	std Z+22, r19 ; set currpid (=parents pid) in r25:r24 of new task
	std Z+21, r1
	std Z+16, r1 ; set the process r1 = 0

	movw ARG0L, r30 ; Save the found stack pointer
	movw r30, r26 ; Restore the saved context pointer
	
	st Z, ARG0L ; Store the new stack pointer in CONTEXT
	std Z+1, ARG0H; Low byte in r25 is stored lowest in memory.
	andi ARG1L, ~STATE_SIGS  ; Mask out STATE_SIGS bit (a set flag would
				 ; disable signal handling)
	std Z+2, ARG1L; Meta byte for process
	sbrs ARG1L, STATE_SLEEPS_BITNO
	clr ARG2L ; If process isnt sleeping, make sure it doesn't have
		  ; a wakeup time
	std Z+3, ARG2L

	; Set up the default signal handler
	ldi r30, lo8(TASK_SIGNALS)
	ldi r31, hi8(TASK_SIGNALS)
	lsl r18
	add r30, r18
	clr r19
	adc r31, r19
	lsr r18

	ldi r24, lo8(pm(SYS_defhandler))
	ldi r25, hi8(pm(SYS_defhandler))
	st Z+, r24
	st Z, r25

	sei

	mov ARG0L, r18  ; Store the PID in r24 and return
	clr ARG0H

SYS_defhandler_end: ; among other things :-)
	ret

; Default signal handler

SYS_defhandler:
	cpi r24, SIG_BREAK
	brne SYS_defhandler_end

SYS_exit:	.global SYS_exit
;	This terminates the calling process

; TODO

	rjmp SYS_exit

SYS_REPRIORITIZE: .global SYS_REPRIORITIZE
	; takes r24 as new priority (0: idle, 1: low, 2: normal, 3: high)
	; and reprioritizes the process with PID r22.
	; Returns 0 on success and -1 on failure (stored in r25:r24)
	;
	; Destroys content in X and Z register
	
	cpi r22, MAX_TASKS
	brsh reprioritize_fail
	ldi r30, ~META_PRIO
	and r30, r24
	brne reprioritize_fail  ; If other bits than the allowed 2 are set,
				; fail
	ldi r30, lo8(TASK_CONTEXT)
	ldi r31, hi8(TASK_CONTEXT)
	lsl r22 ; multiply pid by 4 to index the TASK_CONTEXT array
	lsl r22
	add r30, r22  ; find the task context of pid r22
	clr r22
	adc r31, r22

	; atomically check if it exists, and if so, reset its priority:
	cli
	ld r26, Z
	ldd r27, Z+1
	adiw r26,0   ; Process must have a nonzero stackpointer
	breq reprioritize_fail
	; all ok, load the process meta byte and write new priority
	ldd r26, Z+2
	andi r26, ~META_PRIO
	or r26, r24
	std Z+2, r26
	sei

	clr r24 ; Signal success
	clr r25
	rjmp reprioritize_done

reprioritize_fail:
	sei
	ser r24
	ser r25

reprioritize_done:
	ret

SYS_GETPID:	.global SYS_GETPID
	; Returns current PID in r25:r24
	lds r24, CURR_PID
	clr r25
	ret

SYS_YIELD: 	.global SYS_YIELD
	; Yields CPU time to other processes

	cli
	push r16
	in r16, SREG  ; Save registers the context switcher doesn't.
	push r16
	push r17
	push r30
	push r31

	rjmp CONTEXT_SWITCH
	; no ret instruction is needed, as the return addresse
	; is allready stored on the stack. The context switcher
	; will reti for us.

SYS_SLEEP:	.global SYS_SLEEP
	; Sleeps for a number of ticks equal to r24
	; destroys r18, r24 and Z

	tst r24
	breq SYS_YIELD   ; dont sleep 0 ticks. Putting task to sleep with
			 ; 0 ticks means it never wakes up!
	lds r18, CURR_PID
	lsl r18 ; Multiply our own PID by 4 to index TASK_CONTEXT
	lsl r18
	ldi r30, lo8(TASK_CONTEXT)
	ldi r31, hi8(TASK_CONTEXT)
	add r30, r18
	clr r18
	adc r31, r18

	cli
	std Z+3, r24    ; Save number of tics to sleep
	ldd r24, Z+2
	ori r24, STATE_SLEEPS ; Mark us as sleeping
	std Z+2, r24

goto_sleep:
	rcall SYS_YIELD ; Yield
	; Check if the process is still sleeping
	; (it could have been woken up by another signal)

	cli
	ldd r24, Z+2
	andi r24, STATE_SLEEPS; If we're not sleeping, ok, return
	breq end_sleep
	; else
	rjmp goto_sleep

end_sleep:
	sei
	ret

SYS_RAISE:	.global SYS_RAISE
	; Raises a signal in a process
	; Signal number in r24 (between 0 and 7)
	; process id in r22

	cpi r24, 8    ; Only accept 7 or less as a signal number.
	brsh no_raise

	ldi r30, lo8(TASK_SIGNALS)
	ldi r31, hi8(TASK_SIGNALS)

	lsl r22   	; Here we just assume that a nonzero pointer to
	add r30, r22	; a signal handler means that the process exists.
	clr r23	  	; It is up to the exit procedure to ensure this.
	adc r31, r23
	lsr r22         ; put r22 back to called state

	cli		; Now we assume the process is running. Keep it that way
	ld r26, Z	; Load signal handler pointer
	ldd r27, Z+1

	adiw r26, 0
	breq no_raise  ; the task in question has no signal handler


	lds r25, CURR_PID	; If we try to raise ourselves
	cp r25, r22		; we should simply call the handler
	brne other_proc 	; No need to save registers, cause
			        ; the calling procedure allready expects
				; r8-r25, r30,31 and r0 to be used.
	sei
	movw r30, r26 ; (the signal number is allready in r24)
	ijmp ; use the current return address (ie. don't use icall)
	
other_proc: 	; Signal another process

	; r22 holds process ID
	; r24 contains signal number

	ldi r30, lo8(SIGNAL_BITMASK)
	ldi r31, hi8(SIGNAL_BITMASK)  ; point Z to the tasks SIGNAL_MASK
	add r30, r22
	clr r23
	adc r31, r23
	
	ldi r26, lo8(TASK_CONTEXT)
	ldi r27, hi8(TASK_CONTEXT)      ; point X to task state array
	lsl r22
	lsl r22
	add r26, r22
	adc r27, r23

	; Find the bitmask of the signal

	ldi r22, 1
try_bitmask:
	tst r24
	breq found_bitmask
	dec r24
	lsl r22
	rjmp try_bitmask

found_bitmask:

	ld r18, Z   ; Signal bitmask
	or r18, r22 ; add the found bit to the bitmask
	st Z, r18

	movw r30, r26 ; TASK_CONTEXT into Z
	ldd r19, Z+2 ; Task meta byte
	ori r19, STATE_SIGPEND ; And signal the process
	std Z+2, r19
no_raise:
	sei
	ret

SYS_START:	.global SYS_START
	; Starts current process up again from SYS_STOP:

	clr r24 ; Flag that we're starting
	cpse r24,r24 ; Skip next instruction

SYS_STOP:	.global SYS_STOP
	; Stops the current process

	ser r24 ; Flag that we're stopping
SYS_STARTSTOP:
	lds r25, CURR_PID
	lsl r25
	lsl r25
	ldi r30, lo8(TASK_CONTEXT)
	ldi r31, lo8(TASK_CONTEXT)
	add r30, r25
	clr r25
	adc r31, r25

	ldd r25, Z+2 ; Task meta byte
	andi r25, ~STATE_STOPPED ; Clear the stop bit
	sbrc r24, 1
	ori r25, STATE_STOPPED  ; set the stop bit if SYS_STOP was called
	std Z+2, r25

	sbrs r24, 1 ; don't call SYS_YIELD on if SYS_START was called
	rjmp SYS_YIELD ; effectively call SYS_YIELD with callee's return
		       ; address
	ret ; return if SYS_START was called
	

SYS_SETSIG:	.global SYS_SETSIG
	; Sets up current tasks signal handler
	; Takes r25:r24 as a pointer to the handler

	lds r23, CURR_PID
	lsl r23
	ldi r30, lo8(TASK_SIGNALS)
	ldi r31, hi8(TASK_SIGNALS)
	add r30, r23
	clr r23
	adc r31, r23

	cli
	st Z+, r24
	st Z, r25
	sei

	ret

