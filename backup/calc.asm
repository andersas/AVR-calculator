.include "defines.h"
.extern init_display
.extern OS_DATA
.extern OS_DATA_END
.org 0x0000
.section .init0
.global _start
_start:
	jmp reset ; power-on-reset
	jmp izero
	jmp ione
	jmp scheduler ; tmr2cmp
	jmp tmr2ovr
	jmp tmr1cmp
	jmp tmr1a
	jmp tmr1b
	jmp tmr1ovr
	jmp tmrzovr
	jmp spi
	jmp uart1
	jmp uart2
	jmp uart3
	jmp adc
	jmp ee_rdy
	jmp ana_cmp
	jmp twi
	jmp inttwo
	jmp tmr0cmp
	jmp spm_rdy

izero:
ione:
tmr2cmp:
tmr2ovr:
tmr1cmp:
tmr1ovr:
tmrzovr:
spi:
uart1:
uart2:
uart3:
adc:
ee_rdy:
ana_cmp:
twi:
inttwo:
spm_rdy:

	; catch stray interrupts
ldi r16, 32
out PORTD, r16
ldi r16, SEG_I
out PORTA, r16
ldi r16, 0b10000000
out PORTC, r16
flaf:
rjmp flaf
	reti

.section .init1

.global tmr1a    ; Keyboard driver
tmr1a:
	; Save context
	push r31
	push r30
	push r16
	in r16, SREG
	push r16

	; Probe each column of the keyboard matrix
	; In order to avoid bouncing, we wait 5 ms before reading
	; PINB, 0-4. Only proceed if the last keypress was in fact read

	lds r16, KEYB_BUF
	cpi r16, KEY_READ
	brne tmr1a_exit
	

	clr r16      ; Set initial OSCR1B value to 5 ms.
	sts KEYBI_COUNT, r16 ; (and initialize internal counter)
	subi r16, -39 ; (TCNT1 was just set to 0 because we entered tmr1a
		      ; interrupt handler.)
	out OCR1BH, r16
	out OCR1BL, r16
	
	in r16, TIMSK          ; Enable the B compare interrupt
	ori r16, (1 << OCIE1B)
	out TIMSK, r16

	; Light up the first column:
	in r16, PORTD
	andi r16, 0b11100000  ; make sure all columns are off at first
	ori r16, 1
	out PORTD, r16

checkloop:

	; sleep for 5 ms

tmr1a_exit:
	pop r16
	out SREG, r16
	pop r16
	pop r30
	pop r31

	reti

.global tmr1b
tmr1b:
	push r31
	push r30
	push r16
	in r16, SREG
	push r16

	in r16, PINB  ; Read the debounced pin configuration
	andi r16, 0b00011111 ; without irrelevant bits
	in r31, PORTD ; and the output configuration
	tst r16
	breq no_luck ; Otherwise if any bits were set: hooray!
	lds r30, KEYBI_COUNT ; Count the event (number of different rows
	inc r30		     ; with keys pressed)
	sts KEYBI_COUNT, r30 

	dec r31 ; Emit a keycode: max value of I/O is 16, sub 1 and
	dec r16 ; we get a 4 bit number
	lsl r31 ; put the output configuration (column) in the top nibble
	lsl r31 ; (while also shoving irrelevant bits out)
	lsl r31
	lsl r31
	or r31, r16  ; Store the input port in in the lower nibble

	sts KEYBI_CODE, r31  ; Store the code (even if more keys than 1 in
			     ; a column has been pressed)

no_luck:              ; Else try the next output pin
	in r30, PORTD ; Switch off the column again

	mov r31, r30  ; Switch off the current port pin
	andi r30, 0b00011111
	com r31
	or r31, r30
	com r31     ; Bit now switched off

	lsl r30     ; Turn on next bit ?
	cpi r30, 17 ; max value is 2^4 = 16
	brsh keybh_done
	
	or r31, r30
	out PORTD, r31

	; Add 5 ms to the timer and wait again:

	in r30, OCR1BL
	in r31, OCR1BH
	subi r30, -39  ; add 5 ms (39 * 5 = 195 < 256, so no carry!)
	out OCR1BH, r31
	out OCR1BL, r30
	rjmp checkloop


keybh_done:
	; Disable all keyboard pins:
	out PORTD, r31

	; Disable tmr1b interrupt again
	in r16, TIMSK
	andi r16, 0xFF - (1 << OCIE1B)
	out TIMSK, r16


	; If any keys were pressed, store them in the key buffer

	lds r16, KEYBI_COUNT  ; Check if we got 1 (and only 1)
			      ; key press
	cpi r16, 1
	brne nokey
	
	; Detected 1 key press. Store that in the keyboard buffer
	lds r30, KEYBI_CODE
	rjmp tmr1a_end
nokey:
	ldi r30, NOKEY ; Keycode for no key pressed is 0x22.
tmr1a_end:

	sts KEYB_BUF, r30
	; Restore context
	pop r16
	out SREG, r16
	pop r16
	pop r30
	pop r31

	reti

.global tmr0cmp  ; Display driver
tmr0cmp:
	; Save registers and status register
	push r15
	push r16
	push r17
	push r30
	push r31
	in r30, SREG
	push r30

	lds r16, ActiveSegment
	inc r16          ;  Work on next segment
	cpi r16, 10	 ; wrap around if the next active segment is 10
	brlo update      ; (there are only segments 0-9)
	clr r16
update:
	; Save the # active segment back again
	sts ActiveSegment, r16

	; Point the Z register to the start of the display buffer
	; to get the output we need to display
	ldi r30, lo8(DISPLAY_DATA)
	ldi r31, hi8(DISPLAY_DATA)
	add r30, r16    ; # of segment to light up
	brcc lightup
	inc r31   ; add 1 to high byte if we got a carry earlier
lightup:
	ld r17, Z ; Segment value
	

	; Prepare for indirect jump into the following jump table:
	ldi r30, lo8(pm(setseg))
	ldi r31, hi8(pm(setseg))


	lsl r16    ; multiply active segment by 2 = endsetseg - setseg
		   ; (the block size of each code segment)

	add r30, r16    ; add 2*segment number to Z
	brcc lightsegs
	inc r31  ; (on carry)
lightsegs:

	in r16, PORTD
	andi r16, 0b00111111
	clr r15
	out PORTD, r16
	out PORTC, r15
	out PORTA, r17
	ijmp ; Go!

setseg:
	sbi PORTC, 0
	rjmp tmr0cmp_end
endsetseg:
	sbi PORTC, 1
	rjmp tmr0cmp_end
	sbi PORTC, 2
	rjmp tmr0cmp_end
	sbi PORTC, 3
	rjmp tmr0cmp_end
	sbi PORTC, 4
	rjmp tmr0cmp_end
	sbi PORTC, 5
	rjmp tmr0cmp_end
	sbi PORTC, 6
	rjmp tmr0cmp_end
	sbi PORTC, 7
	rjmp tmr0cmp_end
	sbi PORTD, 6
	rjmp tmr0cmp_end
	sbi PORTD, 7
	rjmp tmr0cmp_end

tmr0cmp_end:

	pop r30
	out SREG, r30
	pop r31
	pop r30
	pop r17
	pop r16	
	pop r15

	reti

reset: ; Initialize all port directions, start timers and load INIT

	cli
	ldi r16, OSCCAL_8MHZ
	out OSCCAL, r16
	ldi r16, hi8(RAMEND) ; Initialize a first stack
	ldi r17, lo8(RAMEND)
	out SPH, r16
	out SPL, r17
	sei

	; Initialize the surrounding hardware handlers
	
	rcall init_diode
	rcall init_sleepmode
	rcall init_display
	rcall init_keyb

	; Start the operating system

	; Initialize data structures

	ldi r30, lo8(OS_DATA)
	ldi r31, hi8(OS_DATA)

	clr r16
	ldi r24, lo8(1000); clear most of RAM
	ldi r25, hi8(1000);
clearmem:
	st Z+, r16
	sbiw r24, 1 
	brne clearmem

	;  Setup the initial task to start from here

	; Store the entry point in r25:24
	ldi r24, lo8(pm(INIT_task))
	ldi r25, hi8(pm(INIT_task))
	ldi r22, PRIO_NORMAL ; Process priority. Not sleeping nor stopped

	ldi r17, lo8(RAMEND-4) ; The new task will be set up with a pointer
	ldi r18, hi8(RAMEND-4) ; to it at the end of stack. This will be
	cli
	out SPL, r17	       ; at RAMEND and RAMEND - 1. It will also
	out SPH, r18	       ; set SREG just after.
			       ; We set our own stack at RAMEND - 4 so
	sei		       ; we can directly RET to the init task.

	rcall SYS_new_process  ; Make us an official process
	ldi r16, 3
	sts TICKS, r16 ; Give the process 3 ticks
	rcall systimer_start  ; Start ticking
	sei  ; Make sure interrupts are enabled
	pop r16
	pop r16
	ret  ; Return to INIT task 


.section .text

init_diode:
	; Set the direction register for port D5 to output
	; and make sure the diode is turned off
	sbi DDRD, 5
	cbi PORTD, 5
	ret

