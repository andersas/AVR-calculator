.include "defines.h"

.section .text

.global disptimer_start

disptimer_start:
	
	; Use the CTC mode of timer 0, and start timer with /1024 prescaler
	;        CTC     with no output on any pins
	ldi r16, (1 << WGM01)
	out TCCR0, r16
	
	; Set the output compare register
	; at 1024 prescaler: 8/1024 MHz * 1.024 ms = 8 cycles

	;ldi r16, 8
	ldi r16, 8
	out OCR0, r16

	; Reset timer value
	clr r16
	out TCNT0, r16

	; Enable the timer0 interrupt
	in r16, TIMSK
	ori r16, (1 << OCIE0)
	out TIMSK, r16

	; Select clock source /1024 prescaler (effectively starts timer). 
	in r16, TCCR0
	ori r16, (1 << CS02) | (1 << CS00)
	andi r16, 0xFF - (1<<CS01)
	out TCCR0, r16

	sei
	ret

.global disptimer_stop
disptimer_stop:

	cli
	; Disable timer interrupt
	in r16, TIMSK
	andi r16, 0xFF - (1<<OCIE0)
	out TIMSK, r16
	sei

	; Stop the timer

	clr r16
	out TCCR0, r16

	; Turn off all the segments and displays
	ldi r16, SEG_OFF
	out PORTA, r16
	clr r16
	out PORTC, r16
	cbi PORTD, 7
	cbi PORTD, 6

	ret

.global keybtimer_start
keybtimer_start:

	cli

	; Make timer1 (16 bit) tick every 125 ms.

	; First enter CTC mode
	
	clr r16 ; (no compare output, forced interrupt or WGM11 WGM12 bits)
	ldi r17, (1 << WGM12) 
	out TCCR1A, r16
	out TCCR1B, r17

	;clr r16  ; Reset timer:
	out TCNT1H, r16
	out TCNT1L, r16

	; Set output compare A register (B does not reset clock).
	; We want 125 ms @ 8 MHz. With a 64 bit prescaler, that gives
	; 15624 ticks (=0x3D08):

	ldi r16, lo8(15624)
	ldi r17, hi8(15624)
	out OCR1AH, r17
	out OCR1AL, r16

	; The keyboard handler needs a 5 ms pause, and relies
	; on OCR1B to be something rather high. We initialize it
	; to 200 here:
	ldi r16, 200
	ldi r17, 0
	out OCR1BH, r17
	out OCR1BL, r16

	; Enable the timer 1A compare output interrupt
	in r16, TIMSK
	ori r16, (1 << OCIE1A)
	out TIMSK, r16

	

	; Start the timer with a /64 prescaler

	in r16,	TCCR1B
	ori r16, (1 << CS11) | (1 << CS10)
	andi r16, 0xFF - (1 << CS12)
	out TCCR1B, r16

	sei

	ret

.global keybtimer_stop
keybtimer_stop:

	cli

	; Disable timer 1A and 1B compare output interrupt
	in r16, TIMSK
	andi r16, 0xFF - ((1 << OCIE1A) | (1 << OCIE1B))
	out TIMSK, r16

	; Stop the timer
	in r16, TCCR1B
	andi r16, 0xFF - ((1 << CS12) | (1 << CS11) | (1 << CS10))
	out TCCR1B, r16

	sei

	ret

.global systimer_start
systimer_start:

	; Clock timer 2 from CLK I/O:

	in r16, ASSR
	ori r16, AS2
	out ASSR, r16

	; Use the CTC mode of timer 2, and start timer with /1024 prescaler
	;        CTC     with no output on any pins
	ldi r16, (1 << WGM21)
	out TCCR2, r16
	
	; Set the output compare register
	; at 1024 prescaler: 8/1024 MHz * 20.48 ms = 160 cycles

	ldi r16, 160
	out OCR2, r16

	; Reset timer value
	clr r16
	out TCNT2, r16

	; Enable the timer0 interrupt
	in r16, TIMSK
	ori r16, (1 << OCIE2)
	out TIMSK, r16

	; Select clock source /1024 prescaler (effectively starts timer). 
	in r16, TCCR2
	ori r16, (1 << CS22) | (1 << CS21) | (1 << CS20)
	out TCCR2, r16

	sei

	ret

.global systimer_stop
systimer_stop:

	cli
	; Disable timer interrupt
	in r16, TIMSK
	andi r16, 0xFF - (1<<OCIE2)
	out TIMSK, r16
	sei

	; Stop the timer

	clr r16
	out TCCR2, r16

	ret
