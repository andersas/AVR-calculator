#ifndef SYSCALLS_H
#define SYSCALLS_H

typedef unsigned char pid_t;

// make a new process pointed to by process, in state state
// and (if state sleeping bit is enabled) sleeping for another ticks ticks.
// Returns pid of new process created or -1 on failure.
pid_t SYS_new_process(void (*process)(pid_t pid), unsigned char state, unsigned char ticks);

// Terminates current process
void __attribute__ ((__noreturn__)) SYS_exit();

// Reprioritizes process with pid pid to priority prio
// prio must be either _PRIO_IDLE, _PRIO_LOW, _PRIO_NORMAL or _PRIO_HIGH.
// Returns -1 on failure, 0 on success.
unsigned char SYS_REPRIORITIZE(unsigned char prio, pid_t pid);

// Returns PID of current process.
pid_t SYS_GETPID();

// Yields CPU time to other processes.
void SYS_YIELD();

// Pauses execution for a number of clock ticks equal to ticks
void SYS_SLEEP(unsigned char ticks);

// Stop the process
void SYS_STOP();
// And start it back up (eg from signal handler)
void SYS_START();

// Sends signal signal to process with pid pid.
// Signal must be a value between 0 and 7
// (system defined: _SIG_CHLD, _SIG_BREAK, _SIG_ALARM)
void SYS_RAISE(unsigned char signal, pid_t pid);

// Set the current tasks signal handler to a function
// pointed to by sighndlr pointer.
// The signal handler must save all registers
// normally saved by C procedures.
void SYS_SETSIG(void (*sighndlr)(unsigned char sig));



#endif
