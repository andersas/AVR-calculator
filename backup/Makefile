sources = $(wildcard *.asm apps/*.asm)
csources = $(wildcard *.c apps/*.c)
objects = $(sources:.asm=.o)
cobjects = $(csources:.c=.o)
chdr = calc.h syscalls.h data.h eeprom.h display_helper.h

target = calc.bin eeprom

all: $(target)

# change to your own mail file here:
calc.bin: calc.out
	avr-objcopy -R .eeprom -O binary calc.out calc.bin
	@du -b calc.bin | awk '{print $$1}' | xargs echo -n "Made"
	@echo " byte target."

eeprom: calc.out
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 -O binary calc.out eeprom

$(objects): %.o : %.asm defines.h
	avr-as -mmcu=atmega16 -o $@ $<

$(cobjects): %.o : %.c $(chdr)
	avr-gcc -Os -frename-registers -mmcu=atmega16 -Wall -c -o $@ $<

calc.h: defines.h
	@echo "Making calc.h"
	@echo "#ifndef CALC_H" > calc.h
	@echo "#define CALC_H" >> calc.h
	@sed 's/\.equ /\#define _/' < defines.h | sed 's/;/\/\//' | sed 's/,//' >> calc.h
	@echo "#endif" >> calc.h

calc.out: $(objects) $(cobjects)
	avr-ld -o $@ $(objects) $(cobjects)


memmap: data.o
	avr-objdump -t data.o | grep .bss | sort | awk -F' ' '{print $$1,": ", $$5}' | grep [A-Z,a-z]
eeprommemmap: eeprom.o
	avr-objdump -t eeprom.o | grep .eeprom | sort | awk -F' ' '{print $$1,": ",$$5}' | grep [A-Z,a-z]

.PHONY: clean
clean:
	rm -f calc.out $(objects) $(cobjects)
.PHONY: distclean
distclean: clean
	rm -f $(target)

.PHONY: transfer
transfer: calc.bin
	avrdude -p m16 -U flash:w:calc.bin:r -P usb -c avrispmkII

.PHONY: getfuses
getfuses:
	avrdude -p m16 -U hfuse:r:hfuse:b -P usb -c avrispmkII
	avrdude -p m16 -U lfuse:r:lfuse:b -P usb -c avrispmkII

.PHONY:	geteeprom
geteeprom:
	avrdude -p m16 -U eeprom:r:eeprom.in:r -P usb -c avrispmkII

.PHONY: seteeprom
seteeprom: eeprom
	avrdude -p m16 -U eeprom:w:eeprom:r -P usb -c avrispmkII

.PHONY: sethfuse
sethfuse: hfuse
	avrdude -p m16 -U hfuse:w:hfuse:b -P usb -c avrispmkII

.PHONY: setlfuse
setlfuse: lfuse
	avrdude -p m16 -U lfuse:w:lfuse:b -P usb -c avrispmkII

.PHONY: fillzero
fillzero:
	avrdude -p m16 -U flash:w:zero:r -P usb -c avrispmkII

