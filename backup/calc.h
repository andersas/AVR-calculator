#ifndef CALC_H
#define CALC_H

// Ram endings

#define _RAMEND 0x045F // 32 registers, 64 I/O ports and 1024 memory locations
#define _STACKLOW 0x03FB  // Lowest stack pointer

// Ram areas

#define _DISPBUF_LEN 10
#define _NBCDREG 6		  // Number of BCD registers
#define _BCDREG_LEN 8


// OS

#define _STACK_DEPTH 81 // 49 ; 49 bytes of stack per process
#define _MAX_TASKS 6 // 10   ; gives (49 - 32 - 1(SREG) - 2 (return addr))  / 2
		     // = 7 nested function calls per process.

#define _MAX_TICKS 20   // number of clock ticks in a process timeslice

#define _META_PRIO 0x03 // bits 0 and 1 gives task priority (in meta byte)
#define _META_STATE 0x0C // bits 2 and 3 give task state
#define _STATE_SLEEPS0x04
#define _STATE_SLEEPS_BITNO 2
#define _STATE_STOPPED 0x08
#define _STATE_WAITS 0x10
#define _STATE_SIGPEND 0x40  // Signals are pending
#define _STATE_SIGPEND_BITNO 6
#define _STATE_SIGS 0x80     // Signals are being handled
#define _STATE_SIGS_BITNO 7

#define _PRIO_IDLE 0
#define _PRIO_LOW 1
#define _PRIO_NORMAL 2
#define _PRIO_HIGH 3

// Signals
#define _SIG_CHLD 0 // Program receives this signal when child dies.
#define _SIG_BREAK 1 // Program receives this signal if interrupted or on ALT+ent.
#define _SIG_ALARM 2 // Program receives this on exiting sleep
#define _SIG_KEYB 3  // Program receives this on keypress update

#define _NSCREENS 4 // Number of virtual screens


// Keyboard
#define _NOKEY 0x22     // Keycode for no key pressed
#define _KEY_READ 0x44 // Signal for the keyboard handler that last key was read.

// 7-segment values:
#define _SEG_OFF 0b11111111
#define _SEG_0 0b11000000
#define _SEG_1 0b11111001
#define _SEG_2 0b10100100
#define _SEG_3 0b10110000
#define _SEG_4 0b10011001
#define _SEG_5 0b10010010
#define _SEG_6 0b10000010
#define _SEG_7 0b11111000
#define _SEG_8 0b10000000
#define _SEG_9 0b10010000
#define _SEG_DOT 0b01111111
#define _SEG_A 0b10001000
#define _SEG_B 0b10000011
#define _SEG_C 0b11000110
#define _SEG_D 0b10100001
#define _SEG_E 0b10000110
#define _SEG_F 0b10001110
#define _SEG_G 0b11000010
#define _SEG_H 0b10001001
#define _SEG_I 0b11111001
#define _SEG_J 0b11100001
#define _SEG_K 0b10010110
#define _SEG_L 0b11000111
#define _SEG_M 0b10101010
#define _SEG_N 0b10101011
#define _SEG_O 0b10100011
#define _SEG_P 0b10001100
#define _SEG_Q 0b10010100
#define _SEG_R 0b10101111
#define _SEG_S 0b10011011
#define _SEG_T 0b10000111
#define _SEG_U 0b11000001
#define _SEG_V 0b11010101
#define _SEG_W 0b10010101
#define _SEG_X 0b10110110
#define _SEG_Y 0b10010001
#define _SEG_Z 0b10101101
#define _SEG_MINUS 0b10111111


// Internal Oscillator calibrator values
#define _OSCCAL_MAX 0xFF
#define _OSCCAL_MIN 0x00
#define _OSCCAL_8MHZ 0xb3

// I/O ports 
#define _PIND 0x10
#define _DDRD 0x11
#define _PORTD 0x12
#define _PINC 0x13
#define _DDRC 0x14
#define _PORTC 0x15
#define _PINB 0x16
#define _DDRB 0x17
#define _PORTB 0x18
#define _PINA 0x19
#define _DDRA 0x1A
#define _PORTA 0x1B
#define _EECR 0x1C
#define _EEDR 0x1D
#define _EEARL 0x1E
#define _EEARH 0x1F

#define _WDTCR 0x21
#define _ASSR 0x22
#define _OCR2 0x23
#define _TCNT2 0x24
#define _TCCR2 0x25
#define _OCR1BL 0x28  // Output compare 1B
#define _OCR1BH 0x29
#define _OCR1AL 0x2A  // Output compare 1A
#define _OCR1AH 0x2B
#define _TCNT1L 0x2C  // Timer/counter 1 counter register
#define _TCNT1H 0x2D
#define _TCCR1B 0x2E // Timer/counter control register 1B / 1A
#define _TCCR1A 0x2F
#define _SFIOR 0x30 // Special function I/O register
#define _OSCCAL 0x31 // OSCillator Calibration
#define _TCNT0 0x32 // Timer 0 current current value
#define _TCCR0 0x33 // Timer 0 control register
#define _MCUCR 0x35
#define _TIMSK 0x39
#define _OCR0 0x3C
#define _SPL 0x3D // Stack
#define _SPH 0x3E 
#define _SREG 0x3F // Status register

// Bit values in certain registers

#define _FOC0 7 // timer 0
#define _WGM00 6
#define _WGM01 3
#define _COM00 4
#define _COM01 5
#define _CS00 0
#define _CS01 1
#define _CS02 2
#define _OCIE0 1 // interrupt bits

#define _WGM12 3  // timer 1
#define _CS10 0
#define _CS11 1
#define _CS12 2
#define _OCIE1A 4
#define _OCIE1B 3
#define _SE 6
#define _SM2 7
#define _SM1 5
#define _SM0 4

#define _WGM20 6 // timer 2
#define _FOC2 7
#define _COM21 5
#define _COM20 4
#define _WGM21 3
#define _CS22 2
#define _CS21 1
#define _CS20 0
#define _OCIE2 7

#define _AS2 3 // Asynchronous timer 2

// EEPROM control register
#define _EERE 0  // Read enable
#define _EEWE 1  // write enable
#define _EEMWE 2  // master write enable
#define _EERIE 3  // ready interrupt

#endif
