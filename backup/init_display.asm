.include "defines.h"
.extern disptimer_start

.section .text

.global init_display
init_display:

	; - Set the data direction registers up
	; - clear buffer areas
	; - start display driver

	; Set directions of relevant ports
	ser r16
	out DDRA, r16
	out DDRC, r16
	sbi DDRD, 7  ; Top two bits in the D register
	sbi DDRD, 6  ; In fact all the bits in DDRD ought to be 1,
		     ; but the other bits are used in other parts of the
		     ; code, so we do it this way for separation
		     ; of concerns
	
	; Set all LED segments to off:
	ldi r16, SEG_OFF
	out PORTA, r16
	; Shut off all segments
	clr r16
	out PORTC, r16
	cbi PORTD, 7
	cbi PORTD, 6

	ldi r16, 9  ; Initialize the tracker of which segment is currently lit
	sts ActiveSegment, r16 

	; Initialize the display buffer:

	ldi r30, lo8(DISPLAY_DATA) ; Z register
	ldi r31, hi8(DISPLAY_DATA)

	ldi r16, SEG_OFF
	ldi r17, DISPBUF_LEN
clear_display:
	st Z+, r16
	dec r17
	brne clear_display

	; Start the display driver
	rcall disptimer_start

	ret


