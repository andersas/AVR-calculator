	.file	"ioroutines.c"
__SREG__ = 0x3f
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__tmp_reg__ = 0
__zero_reg__ = 1
	.global __do_copy_data
	.global __do_clear_bss
	.text
.global	screenSwitch
	.type	screenSwitch, @function
screenSwitch:
/* prologue: function */
/* frame size = 0 */
	cpi r24,lo8(4)
	brsh .L5
	ldi r18,lo8(0)
	ldi r19,hi8(0)
.L3:
	lds r22,CURR_SCREEN
	movw r30,r18
	subi r30,lo8(-(DISPLAY_DATA))
	sbci r31,hi8(-(DISPLAY_DATA))
	ld r25,Z
	mov r20,r22
	ldi r21,lo8(0)
	movw r30,r20
	lsl r30
	rol r31
	ldi r22,3
1:	lsl r20
	rol r21
	dec r22
	brne 1b
	add r30,r20
	adc r31,r21
	add r30,r18
	adc r31,r19
	subi r30,lo8(-(SCREENS))
	sbci r31,hi8(-(SCREENS))
	st Z,r25
	subi r18,lo8(-(1))
	sbci r19,hi8(-(1))
	cpi r18,10
	cpc r19,__zero_reg__
	brne .L3
	sts CURR_SCREEN,r24
	ldi r24,lo8(0)
	ldi r25,hi8(0)
.L4:
	lds r20,CURR_SCREEN
	mov r18,r20
	ldi r19,lo8(0)
	movw r30,r18
	lsl r30
	rol r31
	ldi r20,3
1:	lsl r18
	rol r19
	dec r20
	brne 1b
	add r30,r18
	adc r31,r19
	add r30,r24
	adc r31,r25
	subi r30,lo8(-(SCREENS))
	sbci r31,hi8(-(SCREENS))
	ld r18,Z
	movw r30,r24
	subi r30,lo8(-(DISPLAY_DATA))
	sbci r31,hi8(-(DISPLAY_DATA))
	st Z,r18
	adiw r24,1
	cpi r24,10
	cpc r25,__zero_reg__
	brne .L4
.L5:
	ret
	.size	screenSwitch, .-screenSwitch
.global	idiv10
	.type	idiv10, @function
idiv10:
/* prologue: function */
/* frame size = 0 */
	ldi r23,lo8(-51)
	mul r24,r23
	movw r24,r0
	clr r1
	mov r24,r25
	lsr r24
	lsr r24
	lsr r24
/* epilogue start */
	ret
	.size	idiv10, .-idiv10
.global	idiv60
	.type	idiv60, @function
idiv60:
/* prologue: function */
/* frame size = 0 */
	ldi r23,lo8(-119)
	mul r24,r23
	movw r24,r0
	clr r1
	mov r24,r25
	swap r24
	lsr r24
	andi r24,lo8(7)
/* epilogue start */
	ret
	.size	idiv60, .-idiv60
.global	getscreen
	.type	getscreen, @function
getscreen:
/* prologue: function */
/* frame size = 0 */
	call SYS_GETPID
	lds r25,SCREEN_PIDS
	cp r25,r24
	brne .L14
	ldi r24,lo8(0)
	ret
.L14:
	lds r18,SCREEN_PIDS+1
	cp r18,r24
	brne .L16
	ldi r24,lo8(1)
	ret
.L16:
	lds r19,SCREEN_PIDS+2
	cp r19,r24
	brne .L17
	ldi r24,lo8(2)
	ret
.L17:
	lds r20,SCREEN_PIDS+3
	cp r20,r24
	breq .L18
	ldi r24,lo8(4)
	ret
.L18:
	ldi r24,lo8(3)
	ret
	.size	getscreen, .-getscreen
.global	dispWrite
	.type	dispWrite, @function
dispWrite:
	push r16
	push r17
/* prologue: function */
/* frame size = 0 */
	mov r17,r24
	mov r16,r22
	call SYS_GETPID
	cpi r17,lo8(10)
	brsh .L24
/* #APP */
 ;  153 "ioroutines.c" 1
	cli
 ;  0 "" 2
/* #NOAPP */
	lds r30,CURR_SCREEN
	ldi r31,lo8(0)
	subi r30,lo8(-(SCREEN_PIDS))
	sbci r31,hi8(-(SCREEN_PIDS))
	ld r25,Z
	cp r25,r24
	brne .L22
	mov r30,r17
	ldi r31,lo8(0)
	subi r30,lo8(-(DISPLAY_DATA))
	sbci r31,hi8(-(DISPLAY_DATA))
	rjmp .L25
.L22:
	call getscreen
	cpi r24,lo8(4)
	breq .L23
	mov r18,r24
	ldi r19,lo8(0)
	movw r30,r18
	lsl r30
	rol r31
	ldi r26,3
1:	lsl r18
	rol r19
	dec r26
	brne 1b
	add r30,r18
	adc r31,r19
	add r30,r17
	adc r31,__zero_reg__
	subi r30,lo8(-(SCREENS))
	sbci r31,hi8(-(SCREENS))
.L25:
	st Z,r16
.L23:
/* #APP */
 ;  166 "ioroutines.c" 1
	sei
 ;  0 "" 2
/* #NOAPP */
.L24:
/* epilogue start */
	pop r17
	pop r16
	ret
	.size	dispWrite, .-dispWrite
.global	wait_keyb
	.type	wait_keyb, @function
wait_keyb:
/* prologue: function */
/* frame size = 0 */
	ldi r24,lo8(68)
	sts KEYB_BUF,r24
	call SYS_GETPID
	mov r30,r24
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(TASK_SIGNALS))
	sbci r31,hi8(-(TASK_SIGNALS))
	std Z+1,__zero_reg__
	st Z,__zero_reg__
	ldi r16,lo8(0)
	ldi r17,lo8(68)
.L46:
/* #APP */
 ;  39 "ioroutines.c" 1
	sei
 ;  0 "" 2
/* #NOAPP */
	lds r25,KEYB_BUF
	cpi r25,lo8(34)
	breq .L27
	cpi r25,lo8(68)
	brne .L28
.L27:
	sts KEYB_BUF,r17
	ldi r24,lo8(2)
	call SYS_SLEEP
	rjmp .L46
.L28:
	sts KEYB_BUF,r17
	mov r24,r25
	andi r24,lo8(15)
	subi r24,lo8(-(1))
	ldi r18,lo8(0)
	rjmp .L30
.L31:
	subi r18,lo8(-(1))
.L30:
	lsr r24
	brne .L31
	swap r25
	andi r25,lo8(15)
	subi r25,lo8(-(1))
	rjmp .L32
.L33:
	subi r24,lo8(-(1))
.L32:
	lsr r25
	brne .L33
	add r16,r18
	tst r24
	brne .L34
	tst r16
	brne .L35
	sbi 50-0x20,5
	ldi r16,lo8(5)
	rjmp .L47
.L35:
	cpi r16,lo8(5)
	brne .L34
	cbi 50-0x20,5
	rjmp .L49
.L34:
	cbi 50-0x20,5
/* #APP */
 ;  79 "ioroutines.c" 1
	cli
 ;  0 "" 2
/* #NOAPP */
	cpi r16,lo8(5)
	brne .L37
	cpi r24,lo8(1)
	brne .L38
	lds r16,CURR_SCREEN
	tst r16
	brne .L39
	ldi r24,lo8(3)
	rjmp .L50
.L39:
	lds r24,CURR_SCREEN
	subi r24,lo8(-(-1))
	rjmp .L50
.L38:
	cpi r24,lo8(2)
	brne .L37
	lds r24,CURR_SCREEN
	subi r24,lo8(-(1))
	andi r24,lo8(3)
.L50:
	call screenSwitch
	rjmp .L49
.L37:
	lds r25,CURR_SCREEN
	cpi r25,lo8(4)
	brsh .L49
	lds r30,CURR_SCREEN
	ldi r31,lo8(0)
	subi r30,lo8(-(SCREEN_PIDS))
	sbci r31,hi8(-(SCREEN_PIDS))
	ld r22,Z
	cpi r22,lo8(11)
	brsh .L49
	tst r24
	brne .L41
	cpi r16,lo8(9)
	brne .L41
	ldi r24,lo8(1)
	rjmp .L48
.L41:
	lds r30,CURR_SCREEN
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	st Z,r24
	lds r30,CURR_SCREEN
	ldi r31,lo8(0)
	lsl r30
	rol r31
	subi r30,lo8(-(KEYPRESS))
	sbci r31,hi8(-(KEYPRESS))
	std Z+1,r16
	ldi r24,lo8(3)
.L48:
	call SYS_RAISE
.L49:
	ldi r16,lo8(0)
	rjmp .L47
.L42:
/* #APP */
 ;  119 "ioroutines.c" 1
	sei
 ;  0 "" 2
/* #NOAPP */
	lds r18,KEYB_BUF
	cpi r18,lo8(34)
	brne .+2
	rjmp .L46
	sts KEYB_BUF,r17
	ldi r24,lo8(1)
	call SYS_SLEEP
.L47:
	lds r19,KEYB_BUF
	cpi r19,lo8(34)
	brne .L42
	rjmp .L46
	.size	wait_keyb, .-wait_keyb
