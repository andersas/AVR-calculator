.include "defines.h"

.section .text

init_keyb: .global init_keyb

	cli
	in r16, DDRD
	ori r16, 0x1F ; 5 columns of buttons
	out DDRD, r16

	in r16, PORTD
	andi r16, 0xE0; turn off all keyboard pins
	out PORTD, r16

	clr r16       ; Configure alle pins on port B as inputs
	out DDRB, r16 ; (bit 0-4 ~ row 0-4)
	
	in r16, PORTB ; set the keyboard input pins in a Tri-state
	andi r16, 0xE0
	out PORTB, r16
	
	sei

	; Start the keyboard driver
	rcall keybtimer_start

	ret

