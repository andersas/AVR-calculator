#ifndef DISPLAY_HELP
#define DISPLAY_HELP

extern unsigned char display_xlate(unsigned char num);
extern void scroll_msg(unsigned char *ptr, unsigned char len, unsigned char delay);

#endif
