.include "defines.h"

.section .eeprom
.org 0x0000



	.byte 'A','n','d','e','r','s'
eep_greet: .global eep_greet
	.byte SEG_O, SEG_Z, SEG_7,SEG_L,SEG_N,SEG_X,SEG_OFF
	.byte SEG_A, SEG_N, SEG_D,SEG_E,SEG_R,SEG_S,SEG_OFF
	.byte SEG_P, SEG_R, SEG_E,SEG_S,SEG_E,SEG_N,SEG_T,SEG_S
	.byte SEG_OFF, SEG_OFF, SEG_A,SEG_V,SEG_R,SEG_MINUS
	.byte SEG_C,SEG_A,SEG_L,SEG_C,SEG_DOT,SEG_DOT,SEG_DOT
	.byte 0

shell_menu: .global shell_menu
	.byte SEG_1, SEG_OFF, SEG_MINUS, SEG_C, SEG_A, SEG_L, SEG_C,SEG_OFF,SEG_OFF,SEG_OFF
	.byte SEG_2, SEG_OFF, SEG_MINUS, SEG_F, SEG_R, SEG_E, SEG_Q,SEG_OFF,SEG_OFF,SEG_OFF
	.byte SEG_3, SEG_OFF, SEG_MINUS, SEG_C, SEG_L, SEG_O, SEG_C, SEG_K, SEG_OFF, SEG_OFF
	.byte SEG_4, SEG_OFF, SEG_MINUS, SEG_R, SEG_E, SEG_S, SEG_E, SEG_T,SEG_OFF,SEG_OFF



eeprom_end: .global eeprom_end

.section .text

eep_rd:	.global eep_rd
	; Read eeprom address r25:r24 into r24

	cli

eep_polleewe:
		sbic EECR, EEWE ; wait until write enable is toggled off
				; or things go wrong
		rjmp eep_polleewe

		out EEARH, r25  ; load address
		out EEARL, r24
		
		sbi EECR, EERE

		in r24, EEDR

	sei
	clr r25
	ret

eep_wr:	.global eep_wr
	; write contents of r22 into eeprom address r25:24
	cli
eep_polleewe2:
		sbic EECR, EEWE ; wait for a previous write to clear off
		rjmp eep_polleewe2

		out EEARH, r25 ; load address
		out EEARL, r24
		; load the data register
		out EEDR, r22
		sbi EECR, EEMWE ; Tell MCU we really want to write..
		sbi EECR, EEWE  ; then write

	sei
	ret

