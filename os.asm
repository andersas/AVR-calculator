.include "defines.h"

.section .text

scheduler:	.global scheduler

	push r16
	in r16, SREG
	push r16
	push r17
	push r30
	push r31

	; Keep track of the system time
	lds r16, SYS_TIME+1
	inc r16
	sts SYS_TIME+1, r16
	brne systime_updated  ; carry into high byte:
	lds r16, SYS_TIME
	inc r16
	sts SYS_TIME, r16

systime_updated:

	; Decrement sleep timers

	ser r16    ; r16 = -1
	ldi r30, lo8(TASK_CONTEXT-2) ; Meta byte of each process, index -1
	ldi r31, hi8(TASK_CONTEXT-2)

updtime:

	inc r16
	cpi r16, MAX_TASKS
	breq wakeup_done  ; Loop termination clause 

	adiw r30, 4 ; size of TASK_CONTEXT array content
	ldd r17, Z+1	; load wakeup time for task r16
	tst r17
	breq updtime
			; wakeup time is nonzero. Decrement it and
	dec r17		; see if it is zero. If so, wake up the task
	std Z+1, r17
	brne updtime    ; wake up task:

	ld r17, Z
	andi r17, ~STATE_SLEEPS  ; Clear the sleeps flag
	ori r17, STATE_SIGPEND   ; mark interrupts pending
	st Z, r17

	ldi r30, lo8(SIGNAL_BITMASK) ; task signal bitmask
	ldi r31, hi8(SIGNAL_BITMASK)
	add r30, r16
	clr r17
	adc r31, r17

	ld r17, Z
	ori r17, (1 << SIG_ALARM) ; run the process alarm signal
	st Z, r17

				     ; Reload
	ldi r30, lo8(TASK_CONTEXT+2) ; meta byte of process
	ldi r31, hi8(TASK_CONTEXT+2)
	mov r17, r16
	lsl r17
	lsl r17
	add r30, r17
	clr r17
	adc r31, r17

	rjmp updtime  ; done

wakeup_done:
	; Decrement tick level

	lds r16, TICKS
	dec r16
	breq CONTEXT_SWITCH ; If the process has exceeded its timeslice
			    ; then preempt current process

	sts TICKS, r16      ; Else store the tick count back and
			    ; return control to current process

	pop r31
	pop r30
	pop r17
	pop r16
	out SREG, r16
	pop r16
	reti
	

.global CONTEXT_SWITCH
CONTEXT_SWITCH:  ; Does not save r16,17,30,31 and SREG !!! (should be done
		 ; by calling process in order r16, SREG, r17,30,31)

	push r18
	push r19
	push r20
	push r21
	push r22
	push r23
	push r24
	push r25
	push r26
	push r27
	push r28
	push r29
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15

	lds r16, CURR_PID  ; Store the current stack pointer
	lsl r16 ; multiply by 4 (= # bytes per element of TASK_CONTEXT)
	lsl r16
	in r17, SPL
	in r18, SPH
	ldi r30, lo8(TASK_CONTEXT)
	ldi r31, hi8(TASK_CONTEXT)
	clr r0
	add r30, r16  ; add CURR_PID offset (*index)
	adc r30, r0   ; r0 contains 0
	st Z, r17  ; and store the pointer low first
	std Z+1, r18

	mov r17, r16 ;  Save starting process

findP:
	; Next, find the process to switch in
	; using round robin scheduling

	subi r16, -4    ; Go up to next task in TASK_CONTEXT array
	cpi r16, 4*MAX_TASKS
	brlo no_rst
	ldi r30, lo8(TASK_CONTEXT-4) ; Wrap around , reset Z pointer
	ldi r31, hi8(TASK_CONTEXT-4)
	clr r16
no_rst:
	cp r16, r17 ; If we've gone through the entire list of processes
		    ; without finding one, try the zero priority processes
	brne proc_check
	clr r0  ; (can't ser r0)
	inc r0
proc_check:
	adiw r30, 4
	ld r28, Z
	ldd r29, Z+1
	adiw r28, 0  ; check if the stack pointer is 0x0000
	breq findP   ; If it is, then it does not represent a task
	ldd r27, Z+2 ; then check if the task is running (meta byte)
		     ; or has signals pending
	mov r26, r27 ; copy r27
	andi r27, STATE_SIGS | STATE_SIGPEND
	brne load_proc
	mov r27, r26
	andi r27, STATE_SLEEPS | STATE_STOPPED | STATE_WAITS
	brne findP
	tst r0 ; If we've been through the lot without finding
	       ; anything to load, load the first idle priority
	       ; process we can find (if r0 = 1 this is so)
	brne load_proc
	mov r27, r26
	andi r27, META_PRIO ; and if is has nonzero priority
	breq findP

load_proc:
	; Found a nonsleeping/nonstopped/interrupted process with nonzero
	; stackpointer.
	; Load it and get out of here!

	; Now r16 = 4*chosen pid, r29:r28 contains stackpointer
	; r26 contains process meta byte

	mov r27, r26
	andi r27, META_PRIO
	; r27 now contains priority of task

	out SPH, r29
	out SPL, r28
	lsr r16  	; Divide index by two again and store it back
	lsr r16
	sts CURR_PID, r16
		        ; Set the number of tics the process lives
			; before being preempted.
			; Priority 0: 20.48 ms  ; idle
			;          1: 61.44 ms  ; low
			;          2: 102.40 ms ; normal
			;          3: 143.36 ms ; high

	lsl r27	; Multiply priority by 2
	subi r27, -1 ; add 1
	sts TICKS, r27 ; Store the # of tics left 

	; Check if any signals are pending:

	ldi r30, lo8(SIGNAL_BITMASK)
	ldi r31, hi8(SIGNAL_BITMASK)
	add r30, r16
	clr r16
	adc r31, r16

	ld r16, Z ; Store for now (until late context restoration)

	; then also check if the process is allready processing signals.
	sbrc r26, STATE_SIGS_BITNO
	clr r16  ; if so, pretend nothing is going on and pop the registers

	pop r15 ; Restore context:
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop r29
	pop r28
	pop r27
	pop r26
	pop r25
	pop r24
	pop r23
	pop r22
	pop r21
	pop r20
	pop r19
	pop r18

	pop r31
	pop r30
	pop r17

	tst r16 ; See if any interrupts are pending
	breq pop_on

	; Signals are waiting!

	rjmp SENDSIG

pop_on:
	pop r16
	out SREG, r16
	pop r16

	reti

SENDSIG:
	push r0   ; Make a call to the signal handler
	push r18  ; and save all registers that procedures are
	push r19  ; expected to use
	push r20
	push r21
	push r22
	push r23
	push r24
	push r25
	push r26
	push r27
	push r30
	push r31

	; Tell the OS that we're in the process of handling
	; signals (eg. so this code won't be called more than
	; once at a time per task)

	lds r20, CURR_PID
	clr r21

SENDSIG_goOn:

	lsl r20 ; index TASK_CONTEXT (4 bytes)
	lsl r20
	ldi r30, lo8(TASK_CONTEXT)
	ldi r31, hi8(TASK_CONTEXT)
	add r30, r20
	adc r31, r21
	ldd r23, Z+2
	ori r23, STATE_SIGS
	std Z+2, r23

	lsr r20 ; Index TASK_SIGNALS (length 2)
	ldi r30, lo8(TASK_SIGNALS) ; Find location of tasks signal handler
	ldi r31, hi8(TASK_SIGNALS)
	add r30, r20
	adc r31, r21

	lsr r20 ; curr pid again
	ld r26, Z
	ldd r27, Z+1
	adiw r26, 0
	breq no_sighndlr

	; Yihuu, the task does have a signal handler. Call it with
	; argument 0 = signal number

	ser r24
	mov r18, r16 ; old r16 (signal bit pattern)
	ser r19    ; new mask for detecting lower bit
find_signo:
	inc r24
	lsl r19    ; Clear lower bit in the detector
	and r16, r19
	cp r16, r18 ; See if the signal pattern has changed
	breq find_signo

	
	ldi r30, lo8(SIGNAL_BITMASK) ; (length 1 so no lsr or lsl)
	ldi r31, hi8(SIGNAL_BITMASK) ; Store the new bitmask
	add r30, r20
	adc r31, r21
	st Z, r16	

	movw r30, r26 ; put the signal handler pointer in Z
	sei	      ; and call the signal handler
	icall
	cli ; Now, new signals could have arrived during the call,
	    ; the signal handler address could have changed
	    ; and all our registers are to be assumed clobbered.
	    ; Start all over again!

	lds r20, CURR_PID
	clr r21

	ldi r30, lo8(SIGNAL_BITMASK) ; (no lsl cause elements have length 1)
	ldi r31, hi8(SIGNAL_BITMASK)
	add r30, r20
	adc r31, r21
	
	ld r16, Z
	tst r16
	brne SENDSIG_goOn ; more signals wait
	
	; No more signals wait, get out of here!

	rjmp end_SENDSIG
	

no_sighndlr: 	; get here if the process has no signal handler
	ldi r30, lo8(SIGNAL_BITMASK)
	ldi r31, hi8(SIGNAL_BITMASK)
	add r30, r20   ;  curr_pid
	adc r31, r21   ;  0

	st Z, r21 ; Clear all interrupt flags
	mov r16, r21 ; and tell the rest of the signal handler this

end_SENDSIG:
	lsl r20 ; TASK_CONTEXT has a size of 4 bytes per element
	lsl r20
	ldi r30, lo8(TASK_CONTEXT) ; Tell the OS that we're no longer
	ldi r31, hi8(TASK_CONTEXT) ; handling signals
	add r30, r20
	adc r31, r21
	ldd r23, Z+2
	andi r23, ~(STATE_SIGS | STATE_SIGPEND)
	std Z+2, r23

	pop r31
	pop r30
	pop r27
	pop r26
	pop r25
	pop r24
	pop r23
	pop r22
	pop r21
	pop r20
	pop r19
	pop r18
	pop r0

	rjmp pop_on


INIT_task:	.global INIT_task ; The initial task 

	; First make this task uninterruptible:

	clr r16
	sts TASK_SIGNALS, r16
	sts TASK_SIGNALS+1, r16

	; Then start the keyboard reader

	ldi r22, PRIO_NORMAL
	ldi r24, lo8(pm(wait_keyb))
	ldi r25, hi8(pm(wait_keyb))
	rcall SYS_new_process

start_shell: ; then shell
	ldi r22, PRIO_NORMAL ; no sleeping etc
	ldi r24, lo8(pm(shell))
	ldi r25, hi8(pm(shell))
	rcall SYS_new_process

	ldi r30, lo8(SCREEN_PIDS) ; Give the shell a screen (# 0)
	ldi r31, hi8(SCREEN_PIDS)
	st Z, r24

	; and then set that screen as the active one:
	clr r25
	sts CURR_SCREEN, r25

	; Reprioritize this process to idle
	; and enter the system idle task:

	rcall SYS_GETPID
	mov r22, r24
	ldi r24, PRIO_IDLE
	rcall SYS_REPRIORITIZE
	rcall SYS_YIELD


idle_task: 	.global idle_task ; This is the system idle task
	nop
	nop  ; Allow for a few interrupts to fire before we go to sleep
	nop
	sleep
	rjmp idle_task

