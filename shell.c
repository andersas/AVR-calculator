#include <avr/io.h>
#include <math.h>

#include "syscalls.h"
#include "data.h"
#include "calc.h"
#include "eeprom.h"
#include "display_helper.h"
#include "ioroutines.h"
#include "apps/bcdcalc.h"
#include "apps/freq.h"
#include "apps/clock.h"

register pid_t bcdcalpid asm("r3");
register pid_t freqpid asm("r4");
register pid_t clockpid asm("r5");
register unsigned char curr_menpoint asm("r6");

void shellsighndlr(unsigned char signo) {

	unsigned char i;

	if (signo == _SIG_KEYB) {
		
		if (KEYPRESS[getscreen()].col == 4 && KEYPRESS[getscreen()].row == 4) {
			// Enter was pressed
			if (bcdcalpid == SYS_GETPID() && curr_menpoint == 0) {

			   bcdcalpid = SYS_new_process(&bcdcalc, _PRIO_HIGH, (unsigned char) 0);

			   // attach screen 1 to the new process
			   SCREEN_PIDS[1] = bcdcalpid;
			   screenSwitch((unsigned char) 1);

			} else if (curr_menpoint == 1 && freqpid == SYS_GETPID()) {
		           freqpid = SYS_new_process(&freq, _PRIO_NORMAL, (unsigned char) 0);
			   // attach to screen 2:
			   SCREEN_PIDS[2] = freqpid;
			   screenSwitch((unsigned char) 2);

			}  else if (curr_menpoint == 2 && clockpid == SYS_GETPID()){
			 clockpid = SYS_new_process(&clock,_PRIO_NORMAL,(unsigned char) 0);
			 SCREEN_PIDS[3] = clockpid;
			 screenSwitch((unsigned char) 3);
			} else if (curr_menpoint == 3) {
		         // SYS_new_process(&reset,_PRIO_NORMAL, (unsigned char) 0);
		         asm ("ldi r16, 0b00001000\n\t"
			 "out %0, r16\n\t"
			 "end: rjmp end\n\t" : : "i" (_WDTCR));

			}
		} else if (KEYPRESS[getscreen()].col == 1 && KEYPRESS[getscreen()].row == 0) {
			if (curr_menpoint == 0) curr_menpoint = 4;
			curr_menpoint--;
			for (i=0;i<_DISPBUF_LEN; dispWrite((unsigned char) i, (unsigned char) eep_rd(&shell_menu[curr_menpoint][i])), i++);

		} else if (KEYPRESS[getscreen()].col == 2 && KEYPRESS[getscreen()].row == 0) {
			curr_menpoint++;
			if (curr_menpoint >= 4) curr_menpoint = 0;
			for (i=0;i<_DISPBUF_LEN; dispWrite((unsigned char) i, (unsigned char) eep_rd(&shell_menu[curr_menpoint][i])), i++);
		}

	}

	return;
}

void __attribute__ ((__noreturn__)) shell(pid_t parpid) {

	unsigned char i;
	unsigned char *ptr;

	// First greet the user with a welcome message:
	
	ptr = (unsigned char *) &BCDREG;
	for (i = 0; (ptr[i] = eep_rd(&eep_greet[i])); i++);

	scroll_msg(ptr, i, 12 /* 22 */);

	// Present the menu:
	curr_menpoint = 0;
	for (i=0;i<_DISPBUF_LEN; dispWrite((unsigned char) i, (unsigned char) eep_rd(&shell_menu[curr_menpoint][i])), i++);

	// Setup our signal handler
	
	SYS_SETSIG(&shellsighndlr);

	bcdcalpid = freqpid = clockpid = SYS_GETPID();

	while (1) {
		SYS_SLEEP((unsigned char) 250);
	}
}


